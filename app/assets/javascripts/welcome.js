// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

function ajax_search(){
	var query = document.getElementById("search-text").value;
	try{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 && xhr.status == 200){
				var array = eval("("+xhr.responseText+")");
				var myObject = JSON.parse(xhr.responseText, null);
				$("#search-text").autocomplete({source: array});
			}
		};
		if(document.getElementById("major_college_value_college").checked){
			xhr.open("GET", "/welcome/ajax_search_college?query="+query, true);
			xhr.send(null);
		}
		else if(document.getElementById("major_college_value_major").checked){
			xhr.open("GET", "/welcome/ajax_search?query="+query, true);
			xhr.send(null);
		}
		
	}
	catch(e){
		alert("Your browser does not support ajax, chri PC akhor");
	}
}
$(function() {
    $(".expand_menu").toggler("fast");
    $(".expand").toggler({initShow: "div.collapse:first"});   
});
