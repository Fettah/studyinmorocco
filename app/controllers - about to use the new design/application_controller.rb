class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
  def confirm_logged_in_college
  	unless session[:college_id] #must be a college admin
  		redirect_to :controller=>"access", :action=>"college_login"
  		return false
  	else
  		return true
  	end
  end

  private
  def confirm_logged_in_admin
    unless session[:id] && session[:super] #must be an admin
      redirect_to :controller=>"access", :action=>"login_admin"
      return false
    else
      return true
    end
  end

  private
  def confirm_logged_in_student
    unless session[:student_id] #must be a student admin
      flash[:notice]= "Please Login to apply for a school"
      redirect_to :controller=>"welcome", :action=>"home"
      return false
    else
      return true
    end
  end
end
