class ApplicationsController < ApplicationController

	require 'zip'

	layout "college_new" , :only=>[:list_applications_for_college, :search_students]
	before_action :confirm_logged_in_student, only: [:create, :list_applications_for_student, :new, :success]
	before_action :confirm_logged_in_college, only: [:list_applications_for_college, :download, :download_all, ]

	def list_applications_for_college
		#you need to do a join here instead of the many databse calls that you have in the view
		@apps = Application.where(:college_id=>session[:college_id])
	end

	def list_applications_for_student
		@applications = Application.where(:student_id=>session[:student_id])
	end

	def ajax_search_students
		query = params[:query].downcase
		result = Student.select(:last_name).where("lower(last_name) like ?","%#{query}%")
		render json: result.map(&:last_name)
	end

	def search_students
		#@result = Application.where(:college_id=>session[:college_id])
		@apps = Application.where(:college_id=>session[:college_id])
	end

	def new
		@college_id = params[:college_id]
		#session[:college_id] = @college_id = params[:college_id]
		@major_id = params[:major_id]
		@student_id = session[:student_id]
		if @student_id #id student is looged in already
			@application = Application.new()
			render :action=>"new"
		else
			#ask the user to login
			#render the modal maybe
			render text: "Please Loggin first"
		end	
	end

	def download
		respond_to do |format|
			format.pdf{
				path = "applications/"+session[:college_id].to_s+"/"+params[:id].to_s+".pdf"
				File.open(path, "r") do |f|
					send_data f.read, :type=> "application/pdf"
				end
			}
		end
	end

	def download_all
		respond_to do |format|
			format.zip{
				folder = "applications/"+session[:college_id].to_s
				zip_files(folder)
			}
			#use this query for statistics for each college
			format.xls{
				sql = "select distinct(students.id), cin,first_name, last_name, students.email, students.phone,major_name, status, applications.created_at 
				from colleges 
				inner join applications on applications.college_id = colleges.id
				inner join students on applications.student_id = students.id 
				inner join majors on applications.major_id = majors.id
				inner join document_statuses on document_statuses.student_id = students.id
				where applications.college_id = '#{session[:college_id]}'
				"
				@result = ActiveRecord::Base.connection.execute(sql)
			}
			format.csv{
				sql = "select distinct(students.id), cin,first_name, last_name, students.email, students.phone,major_name, status, applications.created_at 
				from colleges 
				inner join applications on applications.college_id = colleges.id
				inner join students on applications.student_id = students.id 
				inner join majors on applications.major_id = majors.id
				inner join document_statuses on document_statuses.student_id = students.id
				where applications.college_id = '#{session[:college_id]}'
				"
				@result = ActiveRecord::Base.connection.execute(sql)
				@students = Student.all
				to_csv(@result) #that block thing can be used here instead
			}
		end
	end

	def create	
		@application = Application.new(application_params)
		#if session[:student_id]# && verify_recaptcha(:Application => @application, :message => "Error with the code entered")
		if application_exist(@application)
			render "already_applied"
		else
			@student = Student.find_by_id(params[:application][:student_id])
			@college = College.find_by_id(params[:application][:college_id])
			@major = Major.find_by_id(params[:application][:major_id])
			#this line was added recently, it may cause some problems
			session[:major_id] = nil
			if !application_for_college_exist(@application)#if Student has already applied to this school=>no need to send documents again
				@college.documents.each do |document|
					DocumentStatus.new(:student_id=>@student.id, :document_id=>document.id).save
				end						
			end

			if @application.save 	
				StudentMailer.new_application(@student, @college, @major, @application.id).deliver	
				redirect_to :action=> "success"
			else
				flash[:notice] = "Problem with saving the record, please retry again"
				render "new"
			end
		end
		#else
			#flash[:notice] = "problem with the serssion or the racaptcha"
			#render "new"
		#end
	end

	def sendSMS(phone_num, text)
		nexmo = Nexmo::Client.new('ad3e9e89', 'e15b7e17')
		nexmo.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		nexmo.send_message!({:to => ""+phone_num+"", :from => 'RUBY', :text => ''+text+''})
		if response.ok?
  			flash[:sms] = "SMS was sent successfully"
		else
  			flash[:sms] = "ERROR SMS was NOT sent"
		end
	end

	def update_status
		app = Application.find_by_id(params[:aid])
		app.status = params[:status].to_s
		if app.save
			@college = College.find_by_id(session[:college_id])
			@student = Student.find_by_id(params[:sid])
			@major = Major.find_by_id(params[:mid])
			StudentMailer.application_status(@student, @college.name, @major.major_name, params[:status].to_s).deliver
			if app.status == "Accepted"	
				phone_num = @student.phone
				text = "Congratulation, You have been accepted at "+ @college.name + " - " + @major.major_name
				sendSMS(phone_num, text)		
			end
			flash[:notice] = "Application status was changed test"
		else
			flash[:notice] = "Application status was not changed"
		end
		redirect_to :controller=>"document_statuses", :action=>"show_docStatus_for_college", :sid=>params[:sid], :aid=>params[:aid], :mid=>params[:mid]
	end

	def success	
	end

	private
	def zip_files(folder)
		zipfile_name = folder+"/application.zip"
		File.delete(zipfile_name) if File.exist?(zipfile_name)
		input_filenames = get_files_names(folder)
		#these two lines will remove a weird dots that appear in the array
		input_filenames.shift
		input_filenames.shift
		Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
			input_filenames.each do |filename|
				zipfile.add(filename, folder + '/' + filename)
			end
		end

		send_file(zipfile_name)
	end

	private
	def get_files_names(folder)
		Dir.entries(folder)
	end

	private
	def application_params
		params.require(:application).permit(:college_id, :major_id, :student_id, :application_pdf)
	end

	private
	def application_exist(appl)
		app = Application.where(:college_id=>appl.college_id, :student_id=>appl.student_id, :major_id=>appl.major_id)
		if app.empty?
			return false
		else
			return true
		end
	end
	
	private
	def application_for_college_exist(appl)
		app = Application.where(:college_id=>appl.college_id, :student_id=>appl.student_id)
		if app.empty?
			return false
		else
			return true
		end
	end

	def inform_student_accepted
		#inform the students when their application is accepted
	end

	def inform_student_rejected
		#inform the students when their application is rejected
	end
	private
	def to_csv(result)
		student_csv = CSV.generate do |csv|
			csv << ["cin","First name", "last name","email","phone", "major name", "application status", "application date"]
			result.each do |row|
				csv << [row["cin"],row["first_name"],row["last_name"],row["email"],row["phone"],row["major_name"],row["status"],row["created_at"]]
				
			end	
		end
		send_data student_csv, :type => 'text/csv'
	end
end

