class DocumentsController < ApplicationController

	layout "college_new"
	before_action :confirm_logged_in_college
	
	def create
		@document = Document.new(document_params)
		@college = College.find_by_id(session[:college_id])
		if @college.documents << @document
			flash[:notice] = "Document was added successfully"
			redirect_to :action=>"list_documents"
		else
			flash[:notice] = "Error: Document was not added"
			redirect_to :action=>"list_documents"
		end
	end
	def list_documents
		@documents = Document.where(:college_id=>session[:college_id])
	end

	def delete
		if Document.find_by_id(params[:id]).destroy
			flash[:notice] = "Docuement was deleted"
		else
			flash[:notice] = "Docuement was NOT deleted"
		end
		redirect_to :action=>"list_documents"
	end

	def search
		query = params[:query].downcase#escape this
		@students = Student.select(:first_name, :last_name).where("lower(first_name) like ? or lower(last_name) like?","%#{query}%", "%#{query}%")
		#@documents = Document.select(:name, :received).joins(:document_statuses).where("college_id = ? and student_id = ?", session[:college_id], student_id)
		render json: @students.map(&:first_name)
		#render json: @result.map(&:received)
	end

	private
	def document_params
		params.require(:document).permit(:name, :id, :college_id)
	end
end
