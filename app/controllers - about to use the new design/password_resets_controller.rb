class PasswordResetsController < ApplicationController

	def create
		student = Student.find_by_email(params[:email])
		college = College.find_by_email(params[:email])
		
		if student 
			student.send_password_reset 
		elsif college
			college.send_password_reset 
		end
		redirect_to root_url, :notice => "password reset instructions were sent"
	end

	def edit
		if @student = Student.find_by_password_reset_token(params[:id])
			render 'edit'
		elsif
			@college = College.find_by_password_reset_token(params[:id])
			render 'edit'
		else 
			render 'edit'	
		end
	end

	def update
	#This needs to be tested, I smel some problems
		if @student = Student.find_by_password_reset_token(params[:id])
			if @student.password_reset_sent_at < 2.hours.ago
				redirect_to new_password_reset_path, :notice=>"password reset has expired"
			elsif @student.update_attributes(student_params)
				redirect_to root_url, :notice=>"password has been reset"
			else
				render "edit"
			end
		elsif @college = College.find_by_password_reset_token(params[:id])
			if @college.password_reset_sent_at < 2.hours.ago
				redirect_to new_password_reset_path, :notice=>"password reset has expired"
			elsif @college.update_attributes(college_params)
				 flash[:notice] = "password has been reset"
				redirect_to :controller=>"access", :action=>"college_login"

			else
				render "edit"
			end
		end
	end
	#NOT taking advantage of DRY, modify it later (for learning purposes)

	def student_params
		params.require(:student).permit(:password, :password_confirmation)
	end
	def college_params
		params.require(:college).permit(:password, :password_confirmation)
	end
end
