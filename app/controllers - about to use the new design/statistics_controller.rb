class StatisticsController < ApplicationController

	layout "college_new", only: [:list_stats_for_college]
	def list_stats_for_college
		gon.college = College.find_by_id(session[:college_id])
		@college = College.find_by_id(session[:college_id])
		@stats_month = Application.select(:student_id).where(:college_id=>session[:college_id])

  	#OPTIMIZATION
  	#anyways, this is working and giving goof result
  	
  	#year = params[:year]
  	#default=2014
  	year = 2014
  	@april = [].push(num_of_applicants(4, year),num_of_accepted(4, year), num_of_accepted(4, year))
    #number of applicants, and accepted applicants from each city

	    sql = "SELECT city, COUNT(*) AS applicants,
				    SUM(case when (status='Accepted') then 1
				else 0
				end) AS accepted_applicants
				FROM STUDENTS JOIN APPLICATIONS ON STUDENTS.ID = APPLICATIONS.STUDENT_ID
				WHERE college_id='#{session[:college_id]}'
				GROUP BY city;"    
	@cities_num_applicants = ActiveRecord::Base.connection.execute(sql)
	@acceptance_rate = (num_of_accepted_per_year(year).to_f/num_of_applicants_per_year(year).to_f * 100).round(2).to_s+" %"
	end

	private
	def num_of_applicants(month, year)
		Application.select(:student_id).where("college_id= ? and extract(month from created_at) = ? and extract(year from created_at) = ?","#{session[:college_id]}", "#{month}", year).distinct.count
	end

	private
	def num_of_accepted(month, year)
		Application.select(:student_id).where("lower(status) = ? and college_id = ? and extract(month from created_at) = ? and extract(year from created_at) = ?", "accepted", "#{session[:college_id]}", month, year).count
	end

	private
	def num_of_applicants_per_year(year)
		Application.select(:student_id).where("college_id= ? and extract(year from created_at) = ?","#{session[:college_id]}", year).distinct.count
	end

	private
	def num_of_accepted_per_year(year)
		Application.select(:student_id).where("lower(status) = ? and college_id = ?  and extract(year from created_at) = ?", "accepted", "#{session[:college_id]}", year).count
	end
end