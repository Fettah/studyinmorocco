class StudentsController < ApplicationController

	def signup
		if session[:student_id]
			redirect_to :controller=>"welcome", :action=>"index" 
		else
			@student = Student.new	
		end
	end

	def create	
		if session[:student_id]	
			redirect_to :controller=>"welcome", :action=>"home" 
		else
			@student = Student.new(student_params)
			if @student.save
				StudentMailer.welcome_student_email(@student).deliver
				flash[:notice] = "Welcome: Account created successfully"
				session[:student_id] = @student.id
			#this will be removed and replaced by redirect to previous page
				redirect_to :controller=>"welcome", :action=>"home"
			else
				render :action=>"signup"
			end
		end
	end

	def edit
		if session[:student_id]
			@student = Student.find_by_id(session[:student_id])
		else
			redirect_to :controller=>"welcome", :action=>"home"
		end
	end

	def update
		if  !session[:student_id]
			redirect_to :controller=>"welcome", :action=>"home"
		else
			@student = Student.find_by_id(session[:student_id])
			if @student.update_attributes(student_update_params)
				flash[:notice] = "Profile updated successfully"
				redirect_to :controller=>"welcome", :action=>"home"
			else
				render "edit"
			end
		end	
	end

	def change_pass
		@student = Student.find_by_id(session[:student_id])
		if params[:old_password].present?
			if @student.authenticate(params[:old_password])
				@student.password = params[:password]
				@student.password_confirmation = params[:password_confirmation]
				if @student.save
					flash[:password] = "Password has been changed successfully"
					redirect_to :controller=>"welcome", :action=>"home"
				else
					flash[:password] = "Password WAS NOT changed"
					render "edit"
				end
			else
				flash[:password] = "Wrong old password"
				render "edit"
			end
		else
			flash[:password] = "old password field is empty"
			render "edit"
		end
		#get the old hash,
		#compare it with the new hash, then store the password
	end

	private
	def student_params
		params.require(:student).permit(:email, :password_digest, :password, :password_confirmation, :gender, :phone, :first_name, :last_name, :city,:address, :nationality, :cin, :birthday)
	end
	private
	def student_update_params
		params.require(:student).permit(:gender, :first_name, :last_name, :city,:address, :nationality, :cin, :birthday, :phone)
	end

end
