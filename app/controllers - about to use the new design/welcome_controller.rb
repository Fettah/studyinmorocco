class WelcomeController < ApplicationController

  def index
    map("welcome/search")
    render "index"
  end
  def ajax_search
    query = params[:query]
    query = query.to_s.downcase.strip
    result = Major.select("major_name").where(["lower(major_name) like ?", "%#{query}%"]).order("major_name ASC").limit(5).uniq 
    render json: result.map(&:major_name)
  end

  def ajax_search_college
    query = params[:query]
    query = query.to_s.downcase.strip
    result = College.where(["lower(name) like ?", "%#{query}%"]).order("name ASC").limit(7).uniq
    colleges = result.map(&:name)
    #abbr = result.map(&:abbreviation)
    #result = colleges.zip(abbr.collect { |x| " ["+ x + "]" }  ).map(&:join)
    render json: colleges
    #the earch above may get changed=>store the value in a table, index, to make the search faster
  end

  def search
    begin  
      initilize
      if(@major_college_value == "college")
        @found_colleges = College.joins(:majors).where(["(lower(name) like ? or lower(abbreviation) like ?) and (lower(city) like ?) and lower(private_public) like ?", "%#{@q}%", "%#{@q}%", "%#{@city}%", "%#{@type}%"]).order('name').uniq(:name)
      else
        @found_majors =   College.joins(:majors).where(["lower(major_name) like ?","%#{@q}%"]).order('name').uniq(:name).page(params[:page]).per_page(20)
      end
      @found_not_like_colleges = College.joins(:majors).where("lower(name) not like ?","#{@q}")
      session[:search] = true
      #the parameter search is a helper that will determine the correct action method to call in the welcome controller
      map("search")
      render "index"
    rescue
      render :file=>"public/404.html"
    end
  end

  def advanced_search
    @bac = params[:bac].downcase
    @type = params[:public_private].downcase
    @city = params[:city].downcase
    @level = params[:level].downcase
    @price_min = params[:price_min]
    @price_max = params[:price_max]
    @cour_du_soir = params[:cour_du_soir]
    if(@price_max.empty? || @price_max.nil?)
      @price_max = 1000000
    end
    #query = "(lower(name) like ? or lower(abbreviation) like ?) and lower(city) like ?"

    @advanced_result = College.where(["LOWER(city) LIKE ? AND private_public = ? AND avec_sans_bac LIKE ?", "%#{@city}%","#{@type}", "%#{@bac}%"]).order('name').uniq(:name)

    #query = "(lower(name) like ? or lower(abbreviation) like ?) and lower(city) like ? and lower(private_public) like ? and avec_sans_bac like ? and level like ? and cour_du_soir like ?"
    #@found_colleges = College.joins(:majors).where(["#{query}", "%#{@q}%", "%#{@q}%", "%#{@city}%", "%#{@type}%", "#{bac}", "#{level}", "#{cour_du_soir}"]).order('name').uniq(:name)
    session[:search] = true
    map("search")
    render "index"

  end

  private
  def map(route)
      @colleges = College.where("latitude  IS NOT NULL and longitude IS NOT NULL")
      @hash = Gmaps4rails.build_markers(@colleges) do |college, marker|
        marker.lat college.latitude
        marker.lng college.longitude
        href_value = route+"?city=&type=&major_college_value=college&q="+college.name+"&commit=Search".html_safe
        info_window_string = '<a class="info_windows">' + college.name + '</a> <br>
                              <a class="btn btn-success" href="'+href_value+'">Apply</a>'
        marker.infowindow info_window_string
      end
  end

  private
  def initilize
    @q = "" 
    @major = params[:major]
    @type = params[:type]
    @q = params[:q].downcase.strip
    @city = params[:city].downcase
    @major_college_value = params[:major_college_value].downcase
  end

end