class AccessController < ApplicationController
	
	#three repeated functions, must be optimized

	#Begin handling student access controll
	def login
		#this will be most likely the index page too
		#since the form will stay there
		#or a modal is needed in all pages as long as the user is not logged in
	end

	def attempt_login
		if params[:email].present? && params[:password].present?
			found_student = Student.where(:email => params[:email]).first
			if found_student
				authorized_student = found_student.authenticate(params[:password])
			end
		end
		if authorized_student
			session[:student_id] = authorized_student.id
			redirect_to :controller=>"welcome", :action=>"home"
		else
			flash[:notice] = "Invalide email et/ou mot de pass"
			#redirect to previous page
			redirect_to :controller=>"welcome", :action=>"home"
		end
	end

	def logout
		session[:student_id] = nil
		redirect_to :controller=>"welcome", :action=>"home"
		flash[:notice] = "you have been logged out"
	end
	#end of handling student access control

	#Begin handling college access controll
	def college_login
		if session[:college_id]
			redirect_to :controller=>"statistics", :action=>"index"
		end
	end
	def attempt_login_college
		if params[:email].present? && params[:password].present?
			college_found = College.where(:email => params[:email]).first
			if college_found
				authorized_college = college_found.authenticate(params[:password])
			end
		end
		if authorized_college
			session[:college_id] = authorized_college.id
			redirect_to :controller=>"statistics", :action=>"index"
		else
			flash[:notice] = "Invalid email or/and password"
			redirect_to :controller=>"access", :action=>"college_login"
		end
	end
	def college_logout
		session[:college_id] = nil
		redirect_to :action=> "college_login"
	end
	 #end of handling college access control

	#Begin handling admin control
	def admin_login	
		#super_id indicating the the logged in user is an admin not a student
		if session[:id] && session[:super]
			redirect_to :controller=>"admins", :action=>"index"
		end	
	end

	def attempt_login_admin
		if params[:email].present? && params[:password].present?
			found_admin = Administrator.where(:email => params[:email]).first
			if found_admin
				authorized_admin = found_admin.authenticate(params[:password])
			end
		end
		if authorized_admin
			#marke the user as logged in
			session[:id] = authorized_admin.id
			session[:super] = true
			#if id don't provide first name this crashs
			#flash[:notice] = "Welcome " + authorized_admin.first_name
			redirect_to :controller=>"admins", :action=>"index"
		else
			flash[:notice] = "Invalid email or/and password"
			#redirect to previous page
			redirect_to :controller=>"access", :action=>"admin_login"
		end
	end

	def logout_admin
		session[:id] = nil
		session[:super] = nil
		redirect_to :controller=>"access", :action=>"admin_login"
		flash[:notice] = "you have been logged out"
	end
	#end of handling admin access control
	
end
