class AdminsController < ApplicationController


	before_action :confirm_logged_in_admin, :except=>[:login, :logout, :attempt_login]
	
	layout "admin"
	
	def index
	end

	def settings
		@admin = Administrator.find_by_id(session[:id])
	end
	
	def update_settings
		@admin = Administrator.find_by_id(session[:id])
		if @admin.update_attributes(admin_params)
			flash[:notice] = "settings were updated successfully"
			render "settings"
		else
			render "settings"
		end
	end

	private
	def admin_params
		params.require(:admin).permit(:first_name, :last_name, :email, :password, :password_confirmation, :password_digest)
	end
	
end