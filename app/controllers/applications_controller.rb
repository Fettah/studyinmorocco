class ApplicationsController < ApplicationController

	require 'zip'

	layout "college_new" , :only=>[:list_applications_for_college, :search_students,:list_of_rejected, :list_of_accepted, :list_of_inProgress, :send_emails]
	before_action :confirm_logged_in_student, only: [:create, :list_applications_for_student, :success]
	before_action :confirm_logged_in_college, only: [:list_applications_for_college, :download, :download_all, ]

	def list_applications_for_college
		@apps = Application.where(:college_id=>session[:college_id]).page(params[:page]).per_page(12)
	end

	def list_applications_for_student
		@applications = Application.where(:student_id=>session[:student_id])
	end

	def ajax_search_students
		query = params[:query].downcase
		result = Student.select(:last_name).where("lower(last_name) like ?","%#{query}%")
		render json: result.map(&:last_name)
	end

	def search_students
		@apps = Application.where(:college_id=>session[:college_id])
	end

	def new
		if session[:student_id]#id student is looged in already
			@college_id = params[:college_id]
			@student_id = session[:student_id]
			@major_id = params[:major_id]
			@application = Application.new()
			render :action=>"new"
		else
			#ask the user to login
			#render the modal maybe
			render "students/ajax_signup_modal"
			#render :text=>"<h3 style='margin-left: 150px'>Please Sign In or Sign Up</h3>"
		end	
	end

	def delete
		Application.find_by_id(params[:id]).destroy
		redirect_to :controller=>"applications", :action=>"list_applications_for_college"
	end
	def list_of_accepted
		@apps = Application.where("lower(status) = ?", "accepted").page(params[:page]).per_page(15)
	end

	def list_of_rejected
		@apps = Application.where("lower(status) = ?", "rejected").page(params[:page]).per_page(15)
	end

	def list_of_inProgress
		@apps = Application.where("lower(status) = ?", "in progress").page(params[:page]).per_page(15)
	end

	def inform_rejected_students	
		begin
			college_name = College.find_by_id(session[:college_id]).name
			apps = Application.where("lower(status) = ?", "rejected")
			emails = []
			apps.each do |app|
				emails.push(Student.select(:email).where(:id=> app.student_id).first.email)
			end
			StudentMailer.rejected_students(college_name,emails).deliver
			flash[:notice] = "Email was sent successfully to all applicants"
			redirect_to :controller=>"applications", :action=>"list_of_rejected"
		rescue
			render :partial=>"failed_to_send_email"
		end
	end

	def inform_accepted_students	
		begin
			college_name = College.find_by_id(session[:college_id]).name
			apps = Application.where("lower(status) = ?", "accepted")
			emails = []
			apps.each do |app|
				emails.push(Student.select(:email).where(:id=> app.student_id).first.email)
			end
			StudentMailer.accepted_students(college_name,emails).deliver
			flash[:notice] = "Email was sent successfully to all applicants"
			redirect_to :controller=>"applications", :action=>"list_of_accepted"
		rescue
			render :partial=>"failed_to_send_email"
		end
	end

	def download
		respond_to do |format|
			format.pdf{
				path = "applications/"+session[:college_id].to_s+"/"+params[:id].to_s+".pdf"
				File.open(path, "r") do |f|
					send_data f.read, :type=> "application/pdf"
				end
			}
		end
	end

	def download_all
		respond_to do |format|
			format.zip{
				folder = "applications/"+session[:college_id].to_s
				zip_files(folder)
			}
			#use this query for statistics for each college
			format.xls{
				sql = "SELECT distinct(students.id), cin,first_name, last_name, students.email, students.phone,major_name, status, applications.created_at 
				FROM colleges 
				INNER JOIN applications ON applications.college_id = colleges.id
				INNER JOIN students ON applications.student_id = students.id 
				INNER JOIN majors ON applications.major_id = majors.id
				INNER JOIN document_statuses ON document_statuses.student_id = students.id
				WHERE applications.college_id = '#{session[:college_id]}'
				"
				@result = ActiveRecord::Base.connection.execute(sql)
			}
			format.csv{
				sql = "SELECT distinct(students.id), cin,first_name, last_name, students.email, students.phone,major_name, status, applications.created_at 
				FROM colleges 
				INNER JOIN applications ON applications.college_id = colleges.id
				INNER JOIN students ON applications.student_id = students.id 
				INNER JOIN majors ON applications.major_id = majors.id
				INNER JOIN document_statuses ON document_statuses.student_id = students.id
				WHERE applications.college_id = '#{session[:college_id]}'
				"
				@result = ActiveRecord::Base.connection.execute(sql)
				@students = Student.all
				to_csv(@result) #that block thing can be used here instead
			}
		end
	end

	def create	
		@application = Application.new(application_params)
		#if session[:student_id]# && verify_recaptcha(:Application => @application, :message => "Error with the code entered")
		if application_exist(@application)
			@college_name = College.find_by_id(params[:application][:college_id]).name
			@major_name = Major.find_by_id(params[:application][:major_id]).major_name
			render "already_applied"
		else
			@student = Student.find_by_id(params[:application][:student_id])
			@college = College.find_by_id(params[:application][:college_id])
			@major = Major.find_by_id(params[:application][:major_id])
			#this line was added recently, it may cause some problems
			session[:major_id] = nil
			if !application_for_college_exist(@application)#if Student has already applied to this school=>no need to send documents again
				@college.documents.each do |document|
					DocumentStatus.new(:student_id=>@student.id, :document_id=>document.id).save
				end						
			end
			if @application.save 	
				StudentMailer.new_application(@student, @college, @major, @application.id).deliver	
				redirect_to :action=> "success"
			else
				flash[:notice] = "Problem with saving the record, please retry again"
				render "new"
			end
		end
		#else
			#flash[:notice] = "problem with the serssion or the racaptcha"
			#render "new"
		#end
	end

	def sendSMS(phone_num, text)
		begin
			nexmo = Nexmo::Client.new('ad3e9e89', 'e15b7e17')
			nexmo.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			nexmo.send_message!({:to => ""+phone_num+"", :from => 'RUBY', :text => ''+text+''})
			if response.ok?
				flash[:sms] = "SMS was sent successfully"
			else
				flash[:sms] = "ERROR SMS was NOT sent"
			end
		rescue
			flash[:sms] = "ERROR SMS was NOT sent"
		end
	end

	def update_status
		app = Application.find_by_id(params[:aid])
		app.status = params[:status].to_s
		if app.save
			@college = College.find_by_id(session[:college_id])
			@student = Student.find_by_id(params[:sid])
			@major = Major.find_by_id(params[:mid])
			StudentMailer.application_status(@student, @college.name, @major.major_name, params[:status].to_s).deliver
			if app.status == "Accepted"	
				phone_num = @student.phone
				text = "Congratulation, You have been accepted at "+ @college.name + " - " + @major.major_name
				sendSMS(phone_num, text)		
			end
			flash[:notice] = "Application status was changed test"
		else
			flash[:notice] = "Application status was not changed"
		end
		redirect_to :controller=>"document_statuses", :action=>"show_docStatus_for_college", :sid=>params[:sid], :aid=>params[:aid], :mid=>params[:mid]
	end

	def success	
	end
	def details
		student = Student.select(:first_name, :last_name, :phone, :cin, :address, :city, :email).find_by_id(params[:sid])
		render :json=>student
	end
	private
	def zip_files(folder)
		zipfile_name = folder+"/application.zip"
		File.delete(zipfile_name) if File.exist?(zipfile_name)
		input_filenames = get_files_names(folder)
		#these two lines will remove a weird dots that appear in the array
		input_filenames.shift
		input_filenames.shift
		Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
			input_filenames.each do |filename|
				zipfile.add(filename, folder + '/' + filename)
			end
		end

		send_file(zipfile_name)
	end

	private
	def get_files_names(folder)
		Dir.entries(folder)
	end

	private
	def application_params
		params.require(:application).permit(:college_id, :major_id, :student_id, :application_pdf)
	end

	private
	def application_exist(appl)
		app = Application.where(:college_id=>appl.college_id, :student_id=>appl.student_id, :major_id=>appl.major_id)
		if app.empty?
			return false
		else
			return true
		end
	end
	
	private
	def application_for_college_exist(appl)
		app = Application.where(:college_id=>appl.college_id, :student_id=>appl.student_id)
		if app.empty?
			return false
		else
			return true
		end
	end

	private
	def to_csv(result)
		student_csv = CSV.generate do |csv|
			csv << ["cin","First name", "last name","email","phone", "major name", "application status", "application date"]
			result.each do |row|
				csv << [row["cin"],row["first_name"],row["last_name"],row["email"],row["phone"],row["major_name"],row["status"],row["created_at"]]
				
			end	
		end
		send_data student_csv, :type => 'text/csv'
	end
end