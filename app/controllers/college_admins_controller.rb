class CollegeAdminsController < ApplicationController

	layout "college_new"
	before_action :confirm_logged_in_college
	def index
		statistics
		render "index"
	end
	
	def edit
		if @college = College.find_by_id(session[:college_id])
			render "edit"
		else
			render :text=>"The path you have specified is invalid"		
		end
	end

	def update
		if !session[:college_id]
			redirect_to :controller=>"welcome", :action=>"home"
		else
			@college = College.find_by_id(session[:college_id])
			if @college.update_attributes(college_admins_params)
				flash[:notice] = "Settings were updated successfully"
				redirect_to :controller=>"college_admins", :action=>"index"
			else
				render "edit"
			end
		end
	end
	
	private
	def college_admins_params
		params.require(:college).permit(:name, :abbreviation, :address, :city, :phone, :fax, :private_public, :description, :scholarship, :college_url)
	end

end
