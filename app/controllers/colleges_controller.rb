class CollegesController < ApplicationController

	#Restrict access to these actions to only logged in Admins
	#before_action :confirm_logged_in_college
	layout "admin"

	def index
	end

	def new_college
		begin
			@college = College.new
		rescue 
			render :file=>"public/404.html"
		end
	end
	#end of new_college
	def create
		begin
			@college = College.new(college_params)
			if @college.save
				flash[:notice] = "college was added successfully"
				redirect_to :controller=>"colleges", :action=>"list"
			else
				render "new_college"
			end
		rescue
			render :file=>"public/404.html"
		end

	end
	#end of create

	def edit
		begin
			@college = College.find_by_id(params[:id])
		rescue
			render :file=>"public/404.html"
		end
	end
	#end of edit
	def update
		begin
			@college = College.find_by_id(params[:id])
			if @college.update_attributes(college_params)
				flash[:notice] = "college was updated successfully"
				redirect_to :controller=>"colleges", :action=>"list"
			else
				render "edit"
			end
		rescue
			render :file=>"public/404.html"
		end
	end
	 #end of update
	def list
		begin
			@colleges = College.order("name").page(params[:page]).per_page(10)
			#try this if the other did not work
		#College.paginate(:page => params[:page], :per_page => 30)
		rescue 
			render :file=>"public/404.html"
		end
	end

	def search
		begin
			@query = params[:q].to_s.downcase
			@colleges = College.order("name").page(params[:page]).per_page(10)
			@colleges = College.where(["lower(name) like ?","%#{@query}%"]).order("name ASC").page(params[:page]).per_page(10)
			render "list"
		rescue 
			render :file=>"public/404.html"
		end
		
	end

	def update_profile
		begin
		@college = College.find_by_id(session[:college_id])
		if @college.update_attributes(college_params)
			flash[:notice] = "profile settings were updated successfully"
			redirect_to :controller=>"college_admins",:action=>"index"
		else
			flash[:notice] = "Could not update profile settings"
			redirect_to :controller=>"college_admins",:action=>"index"
		end
		rescue
			render :file=>"public/404.html"
		end
	end

	def delete
		begin
			College.find_by_id(params[:id]).destroy
			flash[:notice] ="College was deleted successfully"
			redirect_to :action=>"list"
		rescue
			render :file=>"public/404.html"
		end
	end

	def import_csv
		if(params[:csv_file].nil?)
			render js: "Please Upload a File"
		else
			College.import(params[:csv_file])
			redirect_to :action=>"list"
		end
		
	end
	private
	def college_params
		params.require(:college).permit(:name, :abbreviation, :email, :password,
			:password_confirmation, :address, :city, :phone, :fax, :private_public, :description, :scholarship, :college_url) 
	end

	private
	def college_params_sans_pass_email
		params.require(:college).permit(:name, :abbreviation,
			:address, :city, :phone, :fax, :private_public, :description, :scholarship) 
	end
end
