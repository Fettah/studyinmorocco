class DocumentStatusesController < ApplicationController
  	
	layout "college_new", :only=>[:show_docStatus_for_college]	
	def show_docStatus_for_student
	  	@documents = Document.select( :name, :received).joins(:document_statuses).where("college_id = ? and student_id = ?", params[:cid], session[:student_id])
	end


	def show_docStatus_for_college
	  	@documents = Document.select("document_statuses.id", :name, :received).joins(:document_statuses).where("college_id = ? and student_id = ?", session[:college_id], params[:sid])
	end

	#will be ajaxed later to change the state of the button
	def update_status
	  	doc = DocumentStatus.find_by_id(params[:status])
	  	if doc.received
	  		doc.received = false
	  		doc.save
	  		redirect_to :action=>"show_docStatus_for_college", :sid=>params[:sid], :aid=>params[:aid], :mid=>params[:mid] 
	  	else
	  		doc.received = true
	  		doc.save
	  		@documents = Document.select("document_statuses.id", :name, :received).joins(:document_statuses).where("college_id = ? and student_id = ?", session[:college_id], params[:sid])
	  		check_all_doc_status
	  		redirect_to :action=>"show_docStatus_for_college", :sid=>params[:sid], :aid=>params[:aid], :mid=>params[:mid] 
	  	end
	end

	def check_all_doc_status
	  	all_received = true
	  	@documents.each do |doc|
	  		if !doc.received
	  			all_received = false
	  			break
	  		end
	  	end
	  	if all_received
	  		student = Student.find_by_id(params[:sid])
	  		college = College.find_by_id(session[:college_id])
	  		StudentMailer.documents_received(student, college).deliver
	  		#send sms, all documents were received
	  	end
	end
end
