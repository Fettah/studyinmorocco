class MajorsController < ApplicationController

	layout "college_new"
	before_action :confirm_logged_in_college
	
	def list_majors
		@majors = Major.all.where(:college_id=>session[:college_id]).order("level").page(params[:page]).per_page(10)
		render "list_majors"
	end

	def create
		@college = College.find_by_id(session[:college_id])
		@major = Major.new(major_params)
		if @college.majors << (@major)
			flash[:notice] = "Major has been added successfully"
			redirect_to  :action=>"list_majors"
		else
			flash[:notice] = "Could not add the major, Needs to be ajaxed"
			redirect_to :action=>"list_majors"
		end
	end

	def update
			@major = Major.find_by_id(params[:major][:id])
			if @major.update_attributes(major_params)
				flash[:notice] = "Major was updated successfully"
				redirect_to :action=>"list_majors"
			else
				flash[:notice] = "Major was NOT updated[empty field or major already exists]"
				redirect_to :action=>"list_majors"
			end 		
	end
	
	def delete
		Major.find_by_id(params[:id]).destroy
		redirect_to :action=>"list_majors"
	end

	def major_params
		params.require(:major).permit(:major_name, :major_url, :level, :field, :cour_du_soir)
	end

end
