class PricesController < ApplicationController

	
	layout "college_new"
	before_action :confirm_logged_in_college
	
	def edit
	end

	def list
		@prices = Price.where(:college_id=>session[:college_id])
	end

	def create
		@college = College.find_by_id(session[:college_id])
		@price = Price.new(price_params)
		if @college.prices << @price
			flash[:notice] = "Price was added successfully"
			redirect_to :action=>"list"
		else
			flash[:notice] = "Could not add the price information"
			redirect_to :action=>"list"
		end
	end

	def delete
		Price.find_by_id(params[:id]).destroy
		flash[:notice] = "Price was deleted"
		redirect_to :action=>"list"
	end

	private
	def price_params
		params.require(:price).permit(:college_id, :amount, :price_type)
	end
end
