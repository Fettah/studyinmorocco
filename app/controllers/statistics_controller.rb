class StatisticsController < ApplicationController

	layout "college_new", only: [:index]

	def index
		gon.college = College.find_by_id(session[:college_id])
    	@college = College.find_by_id(session[:college_id])
    	@stats_month = Application.select(:student_id).where(:college_id=>session[:college_id])

    	#current year: this will be retrived from the url next year
    	year = 2014
    	
    	monthly_statistics(year)
	    sql = "SELECT city, COUNT(*) AS applicants,
	    	    SUM(case when (status='Accepted' or status = 'accepted') then 1
	    	    else 0
	    	    end) AS accepted_applicants
	    	    FROM STUDENTS JOIN APPLICATIONS ON STUDENTS.ID = APPLICATIONS.STUDENT_ID
	    	    WHERE college_id='#{session[:college_id]}'
	    	    GROUP BY city;"    
	    @cities_num_applicants = ActiveRecord::Base.connection.execute(sql)
    	@acceptance_rate = (num_of_accepted_per_year(year).to_f/num_of_applicants_per_year(year).to_f * 100).round(2).to_s+" %"
	end

	private
	def monthly_statistics(year)
		@jan = num_of_applicants(1, year)
		@feb = num_of_applicants(2, year)
		@mars = num_of_applicants(3, year)
		@april = num_of_applicants(4, year)
		@mai = num_of_applicants(5, year)
  		@jun = num_of_applicants(6, year)
  		@july = num_of_applicants(7, year)
  		@aug = num_of_applicants(8, year)
  		@sept = num_of_applicants(9, year)
  		@oct = num_of_applicants(10, year)
  		@nov = num_of_applicants(11, year)
  		@dec = num_of_applicants(12, year)
	end

	private
	def num_of_applicants(month, year)
		Application.select(:student_id).where("college_id= ? and extract(month from created_at) = ? and extract(year from created_at) = ?","#{session[:college_id]}", "#{month}", year).distinct.count
	end

	private
	def num_of_accepted(month, year)
		Application.select(:student_id).where("lower(status) = ? and college_id = ? and extract(month from created_at) = ? and extract(year from created_at) = ?", "accepted", "#{session[:college_id]}", month, year).distinct.count
	end

	private
	def num_of_rejected(month, year)
		Application.select(:student_id).where("lower(status) = ? and college_id = ? and extract(month from created_at) = ? and extract(year from created_at) = ?", "rejected", "#{session[:college_id]}", month, year).distinct.count
	end

	private
	def num_of_applicants_per_year(year)
		Application.select(:student_id).where("college_id= ? and extract(year from created_at) = ?","#{session[:college_id]}", year).distinct.count
	end

	private
	def num_of_accepted_per_year(year)
		Application.select(:student_id).where("lower(status) = ? and college_id = ?  and extract(year from created_at) = ?", "accepted", "#{session[:college_id]}", year).count
	end
	
	#return the number of accepteed student in each major
	private
	def num_of_accepted_per_major(year)
		Application.select(:student_id).where("lower(status) = ? and college_id = ?  and extract(year from created_at) = ?", "accepted", "#{session[:college_id]}", year).count
	end

	#return the number of applicants in each major
	private
	def num_of_applicants_per_major(year)
		Application.select(:student_id).where("college_id= ? and extract(year from created_at) = ?","#{session[:college_id]}", year).distinct.count
	end

end