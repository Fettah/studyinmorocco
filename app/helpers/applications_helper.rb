module ApplicationsHelper
	
	def check_progress(sid,cid,mid)
		documents = Document.select("document_statuses.id", :name, :received).joins(:document_statuses).where("college_id = ? and student_id = ?", cid, sid)
		all_received = true
	  	documents.each do |doc|
	  		if !doc.received
	  			all_received = false
	  			break
	  		end
	  	end
	  	if app_status(sid, cid, mid, "Accepted")
	  		return "100"
	  	elsif app_status(sid, cid, mid, "Rejected")
	  		return "Rejected"
	  	elsif all_received
	  		return "66"
	  	else
	  		return "33"
	  	end
	end

	private
	def app_status(sid, cid, mid,status)
		if !Application.where("college_id = ? and student_id = ? and major_id = ? and status = ?", cid, sid, mid,"#{status}").empty?
			return true
		else 
			return false
		end
	end
end
