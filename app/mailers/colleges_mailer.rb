class CollegesMailer < ActionMailer::Base
  default from: "etudeinmorocco@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.colleges_mailer.new_application.subject
  #
  def new_application(college)

    @college = college
    mail to: college.email, subject: "new application has been submitted"
  end
end
