class StudentMailer < ActionMailer::Base
  default from: "etudeinmorocco@gmail.com"

  def welcome_student_email(student)
  	@student = student
  	mail(to: @student.email, subject: "Welcome to Study In Morocco")
  end
  
  def password_reset(student)
  	@student = student
  	mail :to=>student.email, :subject=>"Password Reset" 	
  end

  def new_application(student, college, major, app_id)

    attachments[student.first_name+'_'+student.last_name+".pdf"] = create_application_pdf(student, college, major, app_id)
    @student = student
    @college = college
    mail :to=>student.email, :bcc=> "instreetsaf@hotmail.com", :subject=>"New application"
  end
  def create_application_pdf(student, college, major, app_id)
    #try catch
    require 'fileutils'
    pdf = Prawn::Document.new
    table_data = 
      [["<i>First name</i>", "<b>#{student.first_name}</b>"],
      ["<i>Last name</i>", "<b>#{student.last_name}</b>"],
      ["<i>CIN</i>", "<b>#{student.cin}</b>"], 
      ["<i>College name </i>", "<b>#{college.name}</b>"], 
      ["<i>Major name</i>", "<b>#{major.major_name}</b>"],
      ["<i>Email</i>", "<b>#{student.email}</b>"],
      ["<i>Phone</i>", "<b>#{student.phone}</b>"],
      ["<i>Date Of Birth</i>", "<b>#{student.birthday}</b>"],
      ["<i>Address</i>", "<b>#{student.address}</b>"],
      ["<i>gender</i>", "<b>#{student.gender}</b>"],
      ["<i>nationality</i>", "<b>#{student.nationality}</b>"],
      ["<i>Date Of Application</i>", "<b>#{Time.now.to_s}</b>"]]
    pdf.text "Study In Morocco", :size=> 30, :style=>:bold, :align=>:center
    pdf.text "College Application", :size=> 10, :style=>:bold, :align=>:center
    pdf.move_down 40
    pdf.table(table_data, :width => 500, :cell_style => { :inline_format => true })
    #this value should be stored in the database
    directory = "applications/"+college.id.to_s+"/"
    FileUtils.mkdir_p directory
    pdf.render_file directory+app_id.to_s+".pdf"
    pdf.render
  end

  def documents_received(student, college)
    @student = student
    @college_name = college.name
    mail(to: @student.email, subjet: "document status: All documents were received")
  end

  def application_status(student, college_name, major_name, status)
    @student = student
    @college_name = college_name
    @major_name = major_name
    @status = status
    mail(to: @student.email, subjet: "Your Application status at"+college_name)
  end

  def rejected_students(college_name,emails)
    @college_name = college_name
    mail(to: emails, subject: "Your application at "+@college_name)
  end
  def accepted_students(college_name,emails)
    @college_name = college_name
    mail(to: emails, subject: "You application at "+ @college_name)
  end
end
