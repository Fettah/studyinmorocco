class Administrator < ActiveRecord::Base
	has_secure_password

	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i

	validates :email, :uniqueness=>true,
				:presence=>true,
				:length=>{:maximum=>70},
				:format=>EMAIL_REGEX
	validates :password, :presence=>true,
				:confirmation=>true
	validates :password_confirmation, :presence=>true
end
