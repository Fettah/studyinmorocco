class Application < ActiveRecord::Base

	belongs_to :student
	belongs_to :college
	belongs_to :major

	validates_presence_of :college_id, :student_id, :major_id

	#before save, append to the xml file?

end