class College < ActiveRecord::Base

	require 'csv'

	has_many :majors
	has_many :documents
	has_many :applications
	has_many :students, :through => :applications
	has_many :prices
	
	geocoded_by :full_address, :if => (:address_changed? || :city_changed?)
	def full_address
		[name, address, city, "Morocco"].compact.join(', ')
	end

	after_validation :geocode
	has_secure_password
	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i
	
	#email update is not working because of this=>email should be unique 
	#but for testing multiple emails are added with the same name
	#that's why the update is not working
	validates :email,#, 	:uniqueness=>true,
	:presence=>true, 
	:length=>{:maximum=>100},
	:format=>EMAIL_REGEX,
	:confirmation=>true

	validates :password, :confirmation=>true, :presence=>true, :on=>:create

	validates :name, :presence=>true, :uniqueness=>true, :length=>{:maximum=>100}
	#to populate the data base easily
	validates :abbreviation, :presence=>true, :length=>{:maximum=>10}
	#validates :address, :presence=>true, :length=>{:maximum=>150}
	#validates :city, :presence=>true
	#validates :phone, :presence=>true, :numericality=>true
	#validates :private_public, :presence=>true
	#validates :description, :length=>{:maximum=>10000}

	
	before_save do
		self.email = self.email.downcase
		self.abbreviation = self.abbreviation.upcase
		self.name = self.name.titleize
	end

	def send_password_reset
		generate_token(:password_reset_token)
		self.password_reset_sent_at = Time.zone.now
		save!
		StudentMailer.password_reset(self).deliver
	end

	def generate_token(column)
		begin
			self[column] = SecureRandom.urlsafe_base64
		end while College.exists?(column => self[column])
	end
	#import data from a cvs
	def self.import(file)
		CSV.foreach(file.path, headers: true) do |row|
			College.create! row.to_hash
		end
	end
end
