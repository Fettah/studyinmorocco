class Document < ActiveRecord::Base

	belongs_to :college
	has_many :document_statuses
	validates :name, :presence=>true, :uniqueness=>true

	before_save do
		self.name = self.name.titleize
	end
end
