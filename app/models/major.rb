class Major < ActiveRecord::Base

	belongs_to :college
	has_many :applications

	validates :major_name, :presence=>true, :length=>{:maximum=>70}
	validates :level, :presence=>true, :length=>{:maximum=>70}
	before_save do
		self.major_name = self.major_name.titleize
	end
end
