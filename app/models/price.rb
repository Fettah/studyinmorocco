class Price < ActiveRecord::Base

	belongs_to :college

	validates :price_type, :presence=>true, :length=>{:maximum=>50}
	validates :amount, :presence=>true, :numericality=>true
end
