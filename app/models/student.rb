class Student < ActiveRecord::Base

	has_many :applications
	has_many :document_statuses
	has_many :colleges, :through => :applications
	
	has_secure_password

	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i
	validates :email,	:uniqueness=>true,
	:presence=>true, 
	:length=>{:maximum=>100},
	:format=>EMAIL_REGEX

	validates :password,:confirmation=>true, :presence=>true, :length=>{:minimum=>6}, :on => :create		
	validates :first_name, :length=>{:maximum=>30}, :presence=>true
	validates :last_name, :length=>{:maximum=>40}, :presence=>true
	validates :cin, :presence=>true, :length=>{:maximum=>10}, :uniqueness=>true #add between here
	validates :city, :length=>{:maximum=>40}, :presence=>true
	validates :gender, :presence=>true
	validates :birthday, :presence=>true
	validates :phone, :presence=>true, :length=>{:maximum=>14}, :numericality=>true
	validates :address, :length=>{:maximum=>150}, :presence=>true
	validates :nationality, :length=>{:maximum=>25}, :presence=>true

	before_save do 
		self.first_name = self.first_name.titleize	
		self.last_name = self.last_name.titleize	
		self.city = self.city.titleize
		self.phone = "212" + self.phone[1..9] if (phone[0..2] != "212"	&& self.phone.length < 11)
	end

	def send_password_reset
		generate_token(:password_reset_token)
		self.password_reset_sent_at = Time.zone.now
		save!
		StudentMailer.password_reset(self).deliver
	end

	def generate_token(column)
		begin
			self[column] = SecureRandom.urlsafe_base64
		end while Student.exists?(column => self[column])
	end
end
