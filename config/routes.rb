StudyInMorocco::Application.routes.draw do
  #test accordion
   
  root 'welcome#home'
  
  #documentStatus
  get "document_statuses/show_docstats_for_student"
  get "document_statuses/show_docStatus_for_college"
  
  #statistics
  get "statistics/list_stats_for_college"
  
  #prices
  get "prices/list"
  get "prices/edit"
  #students routes
  get "students/signup"
  get "students/create"
  get "students/edit"
  get "students/update"
  get "students/change_pass"
  get "students/signin"

  #welcome
  get "welcome/search"
  get "welcome/index" 
  get "welcome/boot"
  get "welcome/ajax_search"
  get "welcome/advanced_search"
  get "welcome/search_by_field"

  #admin controller
  get "admin", to: "access#admin_login"
  get "admins/settings"
  get "admins/update"

  #acess controller
  get "access/login"
  get "access/logout"

  #colleges_admins
  get "college_admins/index"
  get "colleges_admins/edit"
  get "colleges_admins/update"
  #college controller
  get "colleges/new_college"
  get "colleges/create"
  get "colleges/search"
  get "colleges/import_csv"
  get "college_login", to: "access#college_login"

  #majors
  get "majors/create"
  get "majors/update"
  get "majors/list_majors"
  
  #documents
  get "documents/list_documents"
  get "documents/create"
  get "documents/process_documents"

  #applications
  get "applications/create"
  get "applications/apply"
  get "application/download"
  get "applications/download_all"
  get "applications/success"
  get "applications/list_applications_for_college"
  get "applications/list_applications_for_student"
  get "applications/acceptance_process"
  get "applications/search_students"
  get "applications/already_applied"
  get "applications/list_of_accepted"
  get "applications/list_of_rejected"
  get "applications/list_of_inProgress"
  get "applications/inform_rejected_students"
  get "applications/inform_accepted_students"
  get "applications/delete"
  get "password_reset/password_reset_sent"
  
  resources :password_resets
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  post ':controller(/:action(/:id(.:format)))'
  get ':controller(/:action(/:id(.:format)))'
end
