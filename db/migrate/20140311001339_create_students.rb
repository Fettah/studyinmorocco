class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
    	t.string "CIN", :limit=>30
    	t.string "first name", :limit=>30
    	t.string "last name"
    	t.string "email", :null=>false
    	t.string "password_digest"
    	t.string "phone", :limit=>14
    	t.string "address", :limit=>100
    	t.string "city", :limit=>40    	
    	t.string "gender", :limit=>8
    	t.date "birthday"
    	t.string "marital status", :limit=>15
    	t.string "nationality"
    	t.timestamps
    end
  end
end
