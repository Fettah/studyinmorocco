class CreateAdministrators < ActiveRecord::Migration
  def change
    create_table :administrators do |t|
    	t.string "first_name", :limit=>70
    	t.string "last_name", :limit=>70
    	t.string "email", :limit=>70
    	t.string "password_hash", :null=>false
        t.timestamps
    end
  end
end
