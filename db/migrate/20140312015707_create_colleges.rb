class CreateColleges < ActiveRecord::Migration
	def change
		create_table :colleges do |t|
			t.string "name", :limit=>120, :null=>false
			t.string "abbreviation", :limit=>50
			t.string "email", :limit=>100
			t.string "password_hash", :null=>false
			t.string "address", :limit=>150
			t.string "city", :limit=>50
			t.string "phone", :limit=>15
			t.string "fax", :limit=>15
			t.string "type", :limit=> 20
			t.date "application_deadline"
			t.text "description", :limit=> 20000
			t.boolean "scholarship"
			t.timestamps
		end
		#removed this because it does not work for postgresql
		#execute "ALTER TABLE colleges AUTO_INCREMENT = 1000"
	end
end