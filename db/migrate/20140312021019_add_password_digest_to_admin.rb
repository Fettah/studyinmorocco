class AddPasswordDigestToAdmin < ActiveRecord::Migration
  def change
  	rename_column :administrators, :password_hash, :password_digest
  end
end
