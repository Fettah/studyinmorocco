class RenamePasswordDigestInColleges < ActiveRecord::Migration
  def change
  	rename_column :colleges, :password_hash, :password_digest
  end
end
