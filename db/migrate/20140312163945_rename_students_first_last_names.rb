class RenameStudentsFirstLastNames < ActiveRecord::Migration
  def change
  	rename_column :students, "first name", :first_name
  	rename_column :students, "last name", :last_name
  end
end
