class RenameTypeInCollege < ActiveRecord::Migration
  def change
  	rename_column :colleges, :type, :private_public
  end
end
