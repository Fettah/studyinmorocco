class CreateMajors < ActiveRecord::Migration
  def change
    create_table :majors do |t|

    	t.string  "major_name", :limit=>100
    	t.integer "college_id"
    	t.timestamps
    end
  end
end
