class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|

    	t.string "name", :limit=>100, :null=>false
    	t.integer "college_id"
    	t.timestamps

    end
  end
end
