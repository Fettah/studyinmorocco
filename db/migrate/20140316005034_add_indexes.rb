class AddIndexes < ActiveRecord::Migration
  
  def change
  	add_index :colleges, :id
  	add_index :students, :id
  	add_index :administrators, :id
  	add_index :majors, :id
  	add_index :documents, :id
  end
end
