class AddNewPasswordResetToCollege < ActiveRecord::Migration
  def change
    add_column :colleges, :password_reset_token, :string
    add_column :colleges, :password_reset_sent_at, :datetime
  end
end
