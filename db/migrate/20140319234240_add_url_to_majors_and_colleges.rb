class AddUrlToMajorsAndColleges < ActiveRecord::Migration

	def change
	 	add_column :colleges, :college_url, :string
	 	add_column :majors, :major_url, :string
	end
	
end
