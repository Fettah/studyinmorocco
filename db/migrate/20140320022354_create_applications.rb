class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
    	t.references :college
    	t.references :student
    	t.references :major
    	t.string  :status
     	t.timestamps
    end
    add_index :applications, ["college_id", "student_id", "major_id"]

  end
end
