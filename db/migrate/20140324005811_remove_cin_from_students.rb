class RemoveCinFromStudents < ActiveRecord::Migration
  def change
  	remove_column :students, :CIN, :string
  	add_column :students, :cin, :string, :limit=>30
  end
end
