class AddLevelAndCourDuSoirToMajor < ActiveRecord::Migration
  def change
  	add_column :majors, :level, :string, :limit=>50
  	add_column :majors, :cour_du_soir, :string, :limit=>20
  end
end