class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
    	t.references :college
    	t.string "price_type", :limit=>50
    	t.string "amount", :limit=>10
    	t.timestamps
    end
    add_index :prices, :id
  end
end
