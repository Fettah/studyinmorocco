class CreateDocumentStatuses < ActiveRecord::Migration
  def change
    create_table :document_statuses do |t|

    	t.references :student
    	t.references :document
    	t.boolean :received, default: false
    	t.timestamps
    end
     add_index :document_statuses, ["student_id", "document_id"]
  end
end
