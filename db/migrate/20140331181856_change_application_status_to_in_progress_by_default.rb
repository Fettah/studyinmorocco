class ChangeApplicationStatusToInProgressByDefault < ActiveRecord::Migration
  def change
  	change_column :applications, :status, :string, :limit=>200, :default=>"in progress"
  end
end
