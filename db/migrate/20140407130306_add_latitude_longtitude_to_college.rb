class AddLatitudeLongtitudeToCollege < ActiveRecord::Migration
  def change
    add_column :colleges, :latitude, :float
    add_column :colleges, :longitude, :float
  end
end
