class AddAvecSansBacToColleges < ActiveRecord::Migration
  def change
    add_column :colleges, :avec_sans_bac, :string, :limit=>10, :default=>"both"
  end
end
