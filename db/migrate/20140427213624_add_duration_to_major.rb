class AddDurationToMajor < ActiveRecord::Migration
  def change
    add_column :majors, :duration, :string, :limit=>40
  end
end
