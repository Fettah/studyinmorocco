class AddFieldToMajors < ActiveRecord::Migration
  def change
    add_column :majors, :field, :string,  :limit=>100
  end
end
