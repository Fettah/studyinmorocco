# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140501160612) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "administrators", force: true do |t|
    t.string   "first_name",      limit: 70
    t.string   "last_name",       limit: 70
    t.string   "email",           limit: 70
    t.string   "password_digest",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "administrators", ["id"], name: "index_administrators_on_id", using: :btree

  create_table "applications", force: true do |t|
    t.integer  "college_id"
    t.integer  "student_id"
    t.integer  "major_id"
    t.string   "status",     limit: 200, default: "in progress"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applications", ["college_id", "student_id", "major_id"], name: "index_applications_on_college_id_and_student_id_and_major_id", using: :btree

  create_table "colleges", force: true do |t|
    t.string   "name",                   limit: 120,                  null: false
    t.string   "abbreviation",           limit: 50
    t.string   "email",                  limit: 100
    t.string   "password_digest",                                     null: false
    t.string   "address",                limit: 150
    t.string   "city",                   limit: 50
    t.string   "phone",                  limit: 15
    t.string   "fax",                    limit: 15
    t.string   "private_public",         limit: 20
    t.date     "application_deadline"
    t.text     "description"
    t.boolean  "scholarship"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "college_url"
    t.string   "price_url",              limit: 500
    t.float    "latitude"
    t.float    "longitude"
    t.string   "avec_sans_bac",          limit: 10,  default: "both"
  end

  add_index "colleges", ["id"], name: "index_colleges_on_id", using: :btree

  create_table "document_statuses", force: true do |t|
    t.integer  "student_id"
    t.integer  "document_id"
    t.boolean  "received",    default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "document_statuses", ["student_id", "document_id"], name: "index_document_statuses_on_student_id_and_document_id", using: :btree

  create_table "documents", force: true do |t|
    t.string   "name",       limit: 200, null: false
    t.integer  "college_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "documents", ["id"], name: "index_documents_on_id", using: :btree

  create_table "majors", force: true do |t|
    t.string   "major_name",   limit: 100
    t.integer  "college_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "major_url"
    t.string   "level",        limit: 50
    t.string   "cour_du_soir", limit: 20
    t.string   "duration",     limit: 40
    t.string   "field",        limit: 100
  end

  add_index "majors", ["id"], name: "index_majors_on_id", using: :btree

  create_table "prices", force: true do |t|
    t.integer  "college_id"
    t.string   "price_type", limit: 50
    t.string   "amount",     limit: 10
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "prices", ["id"], name: "index_prices_on_id", using: :btree

  create_table "students", force: true do |t|
    t.string   "first_name",             limit: 30
    t.string   "last_name"
    t.string   "email",                              null: false
    t.string   "password_digest"
    t.string   "phone",                  limit: 14
    t.string   "address",                limit: 100
    t.string   "city",                   limit: 40
    t.string   "gender",                 limit: 8
    t.date     "birthday"
    t.string   "marital status",         limit: 15
    t.string   "nationality"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "cin",                    limit: 30
  end

  add_index "students", ["id"], name: "index_students_on_id", using: :btree

end
