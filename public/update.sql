PGDMP     8                    r           SIM_development    9.3.3    9.3.3 K    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    16393    SIM_development    DATABASE     �   CREATE DATABASE "SIM_development" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
 !   DROP DATABASE "SIM_development";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            �           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5            �            3079    11750    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    187            �            1259    16396    administrators    TABLE     2  CREATE TABLE administrators (
    id integer NOT NULL,
    first_name character varying(70),
    last_name character varying(70),
    email character varying(70),
    password_digest character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
 "   DROP TABLE public.administrators;
       public         postgres    false    5            �            1259    16394    administrators_id_seq    SEQUENCE     w   CREATE SEQUENCE administrators_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.administrators_id_seq;
       public       postgres    false    5    171            �           0    0    administrators_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE administrators_id_seq OWNED BY administrators.id;
            public       postgres    false    170            �            1259    40979    applications    TABLE     !  CREATE TABLE applications (
    id integer NOT NULL,
    college_id integer,
    student_id integer,
    major_id integer,
    status character varying(200) DEFAULT 'in progress'::character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
     DROP TABLE public.applications;
       public         postgres    false    5            �            1259    40977    applications_id_seq    SEQUENCE     u   CREATE SEQUENCE applications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.applications_id_seq;
       public       postgres    false    182    5            �           0    0    applications_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE applications_id_seq OWNED BY applications.id;
            public       postgres    false    181            �            1259    16404    colleges    TABLE     q  CREATE TABLE colleges (
    id integer NOT NULL,
    name character varying(120) NOT NULL,
    abbreviation character varying(50),
    email character varying(100),
    password_digest character varying(255) NOT NULL,
    address character varying(150),
    city character varying(50),
    phone character varying(15),
    fax character varying(15),
    private_public character varying(20),
    application_deadline date,
    description text,
    scholarship boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    password_reset_token character varying(255),
    password_reset_sent_at timestamp without time zone,
    college_url character varying(255),
    price_url character varying(500),
    latitude double precision,
    longitude double precision,
    avec_sans_bac character varying(10) DEFAULT 'both'::character varying
);
    DROP TABLE public.colleges;
       public         postgres    false    5            �            1259    16402    colleges_id_seq    SEQUENCE     q   CREATE SEQUENCE colleges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.colleges_id_seq;
       public       postgres    false    173    5            �           0    0    colleges_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE colleges_id_seq OWNED BY colleges.id;
            public       postgres    false    172            �            1259    57356    document_statuses    TABLE     �   CREATE TABLE document_statuses (
    id integer NOT NULL,
    student_id integer,
    document_id integer,
    received boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
 %   DROP TABLE public.document_statuses;
       public         postgres    false    5            �            1259    57354    document_statuses_id_seq    SEQUENCE     z   CREATE SEQUENCE document_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.document_statuses_id_seq;
       public       postgres    false    5    186            �           0    0    document_statuses_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE document_statuses_id_seq OWNED BY document_statuses.id;
            public       postgres    false    185            �            1259    32781 	   documents    TABLE     �   CREATE TABLE documents (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    college_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.documents;
       public         postgres    false    5            �            1259    32779    documents_id_seq    SEQUENCE     r   CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.documents_id_seq;
       public       postgres    false    180    5            �           0    0    documents_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE documents_id_seq OWNED BY documents.id;
            public       postgres    false    179            �            1259    24578    majors    TABLE     }  CREATE TABLE majors (
    id integer NOT NULL,
    major_name character varying(100),
    college_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    major_url character varying(255),
    level character varying(50),
    cour_du_soir character varying(20),
    duration character varying(40),
    field character varying(100)
);
    DROP TABLE public.majors;
       public         postgres    false    5            �            1259    24576    majors_id_seq    SEQUENCE     o   CREATE SEQUENCE majors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.majors_id_seq;
       public       postgres    false    5    178            �           0    0    majors_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE majors_id_seq OWNED BY majors.id;
            public       postgres    false    177            �            1259    49183    prices    TABLE     �   CREATE TABLE prices (
    id integer NOT NULL,
    college_id integer,
    price_type character varying(50),
    amount character varying(10),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.prices;
       public         postgres    false    5            �            1259    49181    prices_id_seq    SEQUENCE     o   CREATE SEQUENCE prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.prices_id_seq;
       public       postgres    false    184    5            �           0    0    prices_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE prices_id_seq OWNED BY prices.id;
            public       postgres    false    183            �            1259    16424    schema_migrations    TABLE     P   CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);
 %   DROP TABLE public.schema_migrations;
       public         postgres    false    5            �            1259    16415    students    TABLE     �  CREATE TABLE students (
    id integer NOT NULL,
    first_name character varying(30),
    last_name character varying(255),
    email character varying(255) NOT NULL,
    password_digest character varying(255),
    phone character varying(14),
    address character varying(100),
    city character varying(40),
    gender character varying(8),
    birthday date,
    "marital status" character varying(15),
    nationality character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    password_reset_token character varying(255),
    password_reset_sent_at timestamp without time zone,
    cin character varying(30)
);
    DROP TABLE public.students;
       public         postgres    false    5            �            1259    16413    students_id_seq    SEQUENCE     q   CREATE SEQUENCE students_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.students_id_seq;
       public       postgres    false    5    175            �           0    0    students_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE students_id_seq OWNED BY students.id;
            public       postgres    false    174            P           2604    16399    id    DEFAULT     h   ALTER TABLE ONLY administrators ALTER COLUMN id SET DEFAULT nextval('administrators_id_seq'::regclass);
 @   ALTER TABLE public.administrators ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    171    170    171            V           2604    40982    id    DEFAULT     d   ALTER TABLE ONLY applications ALTER COLUMN id SET DEFAULT nextval('applications_id_seq'::regclass);
 >   ALTER TABLE public.applications ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    181    182    182            Q           2604    16407    id    DEFAULT     \   ALTER TABLE ONLY colleges ALTER COLUMN id SET DEFAULT nextval('colleges_id_seq'::regclass);
 :   ALTER TABLE public.colleges ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    172    173    173            Y           2604    57359    id    DEFAULT     n   ALTER TABLE ONLY document_statuses ALTER COLUMN id SET DEFAULT nextval('document_statuses_id_seq'::regclass);
 C   ALTER TABLE public.document_statuses ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    185    186    186            U           2604    32784    id    DEFAULT     ^   ALTER TABLE ONLY documents ALTER COLUMN id SET DEFAULT nextval('documents_id_seq'::regclass);
 ;   ALTER TABLE public.documents ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    180    179    180            T           2604    24581    id    DEFAULT     X   ALTER TABLE ONLY majors ALTER COLUMN id SET DEFAULT nextval('majors_id_seq'::regclass);
 8   ALTER TABLE public.majors ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    177    178    178            X           2604    49186    id    DEFAULT     X   ALTER TABLE ONLY prices ALTER COLUMN id SET DEFAULT nextval('prices_id_seq'::regclass);
 8   ALTER TABLE public.prices ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    183    184    184            S           2604    16418    id    DEFAULT     \   ALTER TABLE ONLY students ALTER COLUMN id SET DEFAULT nextval('students_id_seq'::regclass);
 :   ALTER TABLE public.students ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    175    174    175            �          0    16396    administrators 
   TABLE DATA               l   COPY administrators (id, first_name, last_name, email, password_digest, created_at, updated_at) FROM stdin;
    public       postgres    false    171   �U                   0    0    administrators_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('administrators_id_seq', 2, true);
            public       postgres    false    170            �          0    40979    applications 
   TABLE DATA               e   COPY applications (id, college_id, student_id, major_id, status, created_at, updated_at) FROM stdin;
    public       postgres    false    182   �V                  0    0    applications_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('applications_id_seq', 316, true);
            public       postgres    false    181            �          0    16404    colleges 
   TABLE DATA               (  COPY colleges (id, name, abbreviation, email, password_digest, address, city, phone, fax, private_public, application_deadline, description, scholarship, created_at, updated_at, password_reset_token, password_reset_sent_at, college_url, price_url, latitude, longitude, avec_sans_bac) FROM stdin;
    public       postgres    false    173   �W                  0    0    colleges_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('colleges_id_seq', 34, true);
            public       postgres    false    172            �          0    57356    document_statuses 
   TABLE DATA               c   COPY document_statuses (id, student_id, document_id, received, created_at, updated_at) FROM stdin;
    public       postgres    false    186   �Y                  0    0    document_statuses_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('document_statuses_id_seq', 124, true);
            public       postgres    false    185            �          0    32781 	   documents 
   TABLE DATA               J   COPY documents (id, name, college_id, created_at, updated_at) FROM stdin;
    public       postgres    false    180   �\                  0    0    documents_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('documents_id_seq', 37, true);
            public       postgres    false    179            �          0    24578    majors 
   TABLE DATA               ~   COPY majors (id, major_name, college_id, created_at, updated_at, major_url, level, cour_du_soir, duration, field) FROM stdin;
    public       postgres    false    178   �]                  0    0    majors_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('majors_id_seq', 74, true);
            public       postgres    false    177            �          0    49183    prices 
   TABLE DATA               U   COPY prices (id, college_id, price_type, amount, created_at, updated_at) FROM stdin;
    public       postgres    false    184   �a                  0    0    prices_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('prices_id_seq', 7, true);
            public       postgres    false    183            �          0    16424    schema_migrations 
   TABLE DATA               -   COPY schema_migrations (version) FROM stdin;
    public       postgres    false    176   �a       �          0    16415    students 
   TABLE DATA               �   COPY students (id, first_name, last_name, email, password_digest, phone, address, city, gender, birthday, "marital status", nationality, created_at, updated_at, password_reset_token, password_reset_sent_at, cin) FROM stdin;
    public       postgres    false    175   ib                  0    0    students_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('students_id_seq', 76, true);
            public       postgres    false    174            \           2606    16401    administrators_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY administrators
    ADD CONSTRAINT administrators_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.administrators DROP CONSTRAINT administrators_pkey;
       public         postgres    false    171    171            l           2606    40984    applications_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.applications DROP CONSTRAINT applications_pkey;
       public         postgres    false    182    182            _           2606    16412    colleges_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY colleges
    ADD CONSTRAINT colleges_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.colleges DROP CONSTRAINT colleges_pkey;
       public         postgres    false    173    173            r           2606    57362    document_statuses_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY document_statuses
    ADD CONSTRAINT document_statuses_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.document_statuses DROP CONSTRAINT document_statuses_pkey;
       public         postgres    false    186    186            i           2606    32786    documents_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.documents DROP CONSTRAINT documents_pkey;
       public         postgres    false    180    180            g           2606    24583    majors_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY majors
    ADD CONSTRAINT majors_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.majors DROP CONSTRAINT majors_pkey;
       public         postgres    false    178    178            p           2606    49188    prices_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.prices DROP CONSTRAINT prices_pkey;
       public         postgres    false    184    184            c           2606    16423    students_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.students DROP CONSTRAINT students_pkey;
       public         postgres    false    175    175            ]           1259    32789    index_administrators_on_id    INDEX     L   CREATE INDEX index_administrators_on_id ON administrators USING btree (id);
 .   DROP INDEX public.index_administrators_on_id;
       public         postgres    false    171            m           1259    40985 <   index_applications_on_college_id_and_student_id_and_major_id    INDEX     �   CREATE INDEX index_applications_on_college_id_and_student_id_and_major_id ON applications USING btree (college_id, student_id, major_id);
 P   DROP INDEX public.index_applications_on_college_id_and_student_id_and_major_id;
       public         postgres    false    182    182    182            `           1259    32787    index_colleges_on_id    INDEX     @   CREATE INDEX index_colleges_on_id ON colleges USING btree (id);
 (   DROP INDEX public.index_colleges_on_id;
       public         postgres    false    173            s           1259    57363 5   index_document_statuses_on_student_id_and_document_id    INDEX        CREATE INDEX index_document_statuses_on_student_id_and_document_id ON document_statuses USING btree (student_id, document_id);
 I   DROP INDEX public.index_document_statuses_on_student_id_and_document_id;
       public         postgres    false    186    186            j           1259    32791    index_documents_on_id    INDEX     B   CREATE INDEX index_documents_on_id ON documents USING btree (id);
 )   DROP INDEX public.index_documents_on_id;
       public         postgres    false    180            e           1259    32790    index_majors_on_id    INDEX     <   CREATE INDEX index_majors_on_id ON majors USING btree (id);
 &   DROP INDEX public.index_majors_on_id;
       public         postgres    false    178            n           1259    49189    index_prices_on_id    INDEX     <   CREATE INDEX index_prices_on_id ON prices USING btree (id);
 &   DROP INDEX public.index_prices_on_id;
       public         postgres    false    184            a           1259    32788    index_students_on_id    INDEX     @   CREATE INDEX index_students_on_id ON students USING btree (id);
 (   DROP INDEX public.index_students_on_id;
       public         postgres    false    175            d           1259    16427    unique_schema_migrations    INDEX     Y   CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);
 ,   DROP INDEX public.unique_schema_migrations;
       public         postgres    false    176            �   �   x�u��k�0 ���_�W��4YNsE�f]Z����]��:,��w��1x����0�u�{���]P�tw�w�;�Z�������(��]����i�(�.\�~�d��j����y�|d*��PL��1�#�4Wc���T�RB�/{�$%�QH�v�����{�^eff���X����uM�����7�y\>�h���r�:{JE_��p͘�I����Bx�dQC      �   �   x�}�Kj1�u���E��9K6a2�d�;�'=�0F�.��.$$ 
E��~.����}}<���vE�P�J9׊N~%!��x_����~�Δ�k;�²a�Vmbw)���B'QX��xjIͬ��k%��Y^������Zd]<���yRR/3��+�b�yV������T9�b-����w4T?��GaѰx�6�4f�R�����QX<,9Y^�#em��.Jo9��iϝ      �     x�uRMo�0=+�����d�iN˶nM�v�ۀ^����R��f��hl�a�����e͖���ک��;g�0D��ly�b	cz��X���΄:��3�_�r��}�?>;a�����m��}���G��_���?��r��ș^�ٕ�Q9X�`��.�Y�%[m�r�މJ4��nD��5����5����G+�=Ѐ�@M�kq��si
G��O�Ƈb��P]{l�V�x�,���&l~������<��69倠M2��
��% �͉ē��;�x
>w=(�_+k���!m�N��u�ľ��uYud�5k�*�:�`t�y����Ļ�r@��ܸ�	ߩ��t�r�I�:��!m��F+��^�2�FTA�@�uAqJk�Gy�Ȕ<-��Ήd��L!o{o�c��t=m(M�FQ��a,�D�
-u��o𴃀��8�X�^zI���E�6�n8KL�U]�MQJ���<_�碩�77�E=_�5/E=��q����v\e�5fR�F�<�`Eëj6��)��ڧ~r�'��_��      �   �  x�}����(E׭(&�߅ I�c���?��e\�B���?s@�'���i����
�)��ү�Q�/Hu@; ���D ���LrN@�/�����%!��D'�����H�/�|Xw*�z�JA���dO����D�������(��a��: [xZ�����f�v쫅�h)[�k�;�('@�`tl,��� r���ݪ��d!D0���*�Q�q����jf;i�ဍ��"�)D0�#����Gvc����B�@�����]V1Ed�%���bZ�-<-�T_"�yy����&��v_�<�rx�G�v
��@��e��)=?D�쀍oU�$iCvB.�ƀ�ZSNW9F`��l�-��9U_����d��o�'`��o��`\oQ�����Y�9�_�/�|Cd�ꄍ���Ui�,���	��J�G%FF�N�h�k����5�/��Ĭ��8��GF`'l\ܵf���7xd;�}���︞���a8!�xjr&�MP'd�ƕ�z�c��	��S^�dHl=��lb�����<2BuB6��(ߊ�N$mM�}ʯOP:�!����o<ܭa�u xdr���]��m12;a�a�FM!����7���f�:�cd�ᄍ�YN�-FF���;TM�w�M�ucX����F�>�12:!���Z4��##T'dO��:bd���⩩�:�bd���b�ZW>�G�� �"��      �     x����N�0���)�u���I�ц�IoUK�]97�Dv� /�ۂ��X�>������ǀ�	g�ADwp�p��?�xJU�ӭm��>��E��<�\�k��\QQTBV�3nDa��(�My5�m߶��_7�o�6�ѻ�#��>uD��8`��9��8g�97I��h&?� \��u���:�x�)�HD����d��)v6^+���!��o�?rH�8\l$�L�o�n�=�+��\��J��������܎�'�Oo��Ek��B�M�G!����t��      �   �  x���_��8Ɵ�S�IH���$��N��d+�c^4 ۪5`�9��ይ0��`�RT����}?Z����H�rW���<���|.��*�*4XUu��YtA�1��y�PBcs�x*���}�������r�K�Td2W���e�)En�Zg���ZT^o��p_:ŰXCaC�a�WM���V�n�J�N���s�X��uQ������N��:}�T�*����K8)=�J�����MP"	�^�#�Ь�����+���JUse��u.��f�����4�x�r�����[�$���!#S�Y����N+�c�!�T��4mQ.9p�@5�Р��4Em� ���l�>��zK�/b8'"s2	р�iu7E�mtò���n!�M��_<	cK�Ȫ6��/;	��V�+xZl��`�a{�}�B����;�ʳj�wQ�"i�vD��t�J�J.9!Q���(����x�Y��%��KXl�£4
��-��� d�sq�Ƹt,��(t֣sI�R��k�`�jrV2�KU�:ʅ%8�8#��O�Ah�r	�X�M44�hh:�N]����@\��sQV[�.��m��,k��9�:��	<�P�Hn!'�5=7���]P����e�k8O�����hL[�q����=�8����P��b��]���K�>����ϲ:�?��OB�y( ��ǉ�\���3�B�)u|�泥���d~����q�O-zW�݋$���8�D�46�)���@86�4�j8�G��c������'~�ф�ƈͽ1Gc0>����dvB6}�Ecl�6��.�=F,	�G��\o㨩��3����M+����N��s`�<�xʧB7��ߜM�-�]�vh���قz�\��S�-A:$�B���Rh��r������      �      x������ � �      �   �   x�M��1�.& uڽ��:�`m�_:(
hP*�>�AT]�p�jO�B�j�i#C���	Ք�K�� j�Ԫ�ƪC�V���a�����$˖�����3��7N�+���*�g1=4$���kU��ٮ�re|?c��VF�      �   Q  x����r�:���)��eG�d��0�ɀR�X`x"!O߇!�tr۷�%Y:U�>�����Xy"܈��>z�܋9���e�n� ��$B?(�A���r"w�h��w����=r�,�޳cO�2�'��uN�I�o72�հRD	ը&S���B�҈g�OiR�D&5B��*x��Ci����(�@�4�ل��T��y#*�Ff7�"��uY�:щ��B�|���4�0k��z<(x���&@>Π��e�#���,�c#����	^�q</�G����(����拏\8I�ް�pFf����x�!b���-s4�X�1)����z���l`�h�lU�9� �P��˨��k?�-�]R"��S'��ۊ���8�:S�_w��ms�*m{�Nl���ю�Ţ�Y����;7�ª��gXO�R��k���w�c�s��I��Z0�3��LbV���ށ�%�C�릈����u�;���pn��Ǻ������#�諳Y���7������}�����"3C3̫�!ps����we� ���x!>C��+�R�:�U��*t����d��1ѩP|����}w2Mc��M�ǷY?-1������vnGK�����y��˥��{�Ԑ��%>��'ia:�"���#��Y�NeE�3+��L�*t��(� ^ �i%�s���w̙n��`r\ʦ�R��j[V]sDo�\^���n�2�Ǯ�'x�̑���P�m*�xX�h&���1ڈOڲ�VX]1�!k�J�Bl��1�j5]{�~r+Hd�����/v՘���^zts��y3k�5��/�ؘs:,��6o>n^z�{W��[9���S"��7V"��,�$pdˇ�^1~��>���9f�ݟ+`����U��M�3S�
]�`p21�����Bȱ�i��1�U0�.~��w�:[=�r鹋4�V����(�qX����1>�V���_����
 �`�j*�F���(8I�Q��b��@uU�:e�����
���UZC6����I�߁��V���������=�߶�H�%~�����lN;�z<νc�x
�lFW͡Ƞ��={%2�����W���
@F�a�+L���_�`�sU���j�5�[I     