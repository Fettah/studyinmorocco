--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.3
-- Started on 2014-03-15 03:11:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 179 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1983 (class 0 OID 0)
-- Dependencies: 179
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 16396)
-- Name: administrators; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE administrators (
    id integer NOT NULL,
    first_name character varying(70),
    last_name character varying(70),
    email character varying(70),
    password_digest character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.administrators OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 16394)
-- Name: administrators_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administrators_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administrators_id_seq OWNER TO postgres;

--
-- TOC entry 1984 (class 0 OID 0)
-- Dependencies: 170
-- Name: administrators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administrators_id_seq OWNED BY administrators.id;


--
-- TOC entry 173 (class 1259 OID 16404)
-- Name: colleges; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE colleges (
    id integer NOT NULL,
    name character varying(120) NOT NULL,
    abbreviation character varying(50),
    email character varying(100),
    password_digest character varying(255) NOT NULL,
    address character varying(150),
    city character varying(50),
    phone character varying(15),
    fax character varying(15),
    private_public character varying(20),
    application_deadline date,
    description text,
    scholarship boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.colleges OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16402)
-- Name: colleges_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE colleges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.colleges_id_seq OWNER TO postgres;

--
-- TOC entry 1985 (class 0 OID 0)
-- Dependencies: 172
-- Name: colleges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE colleges_id_seq OWNED BY colleges.id;


--
-- TOC entry 178 (class 1259 OID 24578)
-- Name: majors; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE majors (
    id integer NOT NULL,
    major_name character varying(100),
    college_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.majors OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 24576)
-- Name: majors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE majors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.majors_id_seq OWNER TO postgres;

--
-- TOC entry 1986 (class 0 OID 0)
-- Dependencies: 177
-- Name: majors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE majors_id_seq OWNED BY majors.id;


--
-- TOC entry 176 (class 1259 OID 16424)
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16415)
-- Name: students; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE students (
    id integer NOT NULL,
    "CIN" character varying(30),
    first_name character varying(30),
    last_name character varying(255),
    email character varying(255) NOT NULL,
    password_digest character varying(255),
    phone character varying(14),
    address character varying(100),
    city character varying(40),
    gender character varying(8),
    birthday date,
    "marital status" character varying(15),
    nationality character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.students OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16413)
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE students_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.students_id_seq OWNER TO postgres;

--
-- TOC entry 1987 (class 0 OID 0)
-- Dependencies: 174
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE students_id_seq OWNED BY students.id;


--
-- TOC entry 1847 (class 2604 OID 16399)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrators ALTER COLUMN id SET DEFAULT nextval('administrators_id_seq'::regclass);


--
-- TOC entry 1848 (class 2604 OID 16407)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colleges ALTER COLUMN id SET DEFAULT nextval('colleges_id_seq'::regclass);


--
-- TOC entry 1850 (class 2604 OID 24581)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY majors ALTER COLUMN id SET DEFAULT nextval('majors_id_seq'::regclass);


--
-- TOC entry 1849 (class 2604 OID 16418)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY students ALTER COLUMN id SET DEFAULT nextval('students_id_seq'::regclass);


--
-- TOC entry 1968 (class 0 OID 16396)
-- Dependencies: 171
-- Data for Name: administrators; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY administrators (id, first_name, last_name, email, password_digest, created_at, updated_at) FROM stdin;
1	Fettah	saadna	saadnafettah@gmail.com	$2a$10$vzRfixmA.OEbtdcQCannwObRaCNompJKBRUPSKTGZ40FiTqc8dmgW	2014-03-14 03:48:00.877027	2014-03-14 05:13:49.721132
\.


--
-- TOC entry 1988 (class 0 OID 0)
-- Dependencies: 170
-- Name: administrators_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administrators_id_seq', 1, true);


--
-- TOC entry 1970 (class 0 OID 16404)
-- Dependencies: 173
-- Data for Name: colleges; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY colleges (id, name, abbreviation, email, password_digest, address, city, phone, fax, private_public, application_deadline, description, scholarship, created_at, updated_at) FROM stdin;
1	Al Akhawayn University	AUI	admission@aui.ma	$2a$10$szP5o6YO8veFYLyq0mWXjeJGGhZRch3N9YVtw91/4xFsXiOkqou/C	Avenue Hassan II	IFRANE	05358522	05358522	public	\N	good	t	2014-03-14 03:40:56.744519	2014-03-14 03:40:56.744519
2	Ecole Hassania des Travaux Publics 	ehtb	admission@ehtb.ma	$2a$10$n0hWehl0pAZ/hUIM4IwH2Os6zRQ4fy0ekpDzQhmetETdnc6a.tQHS	Boulevard Mohamed 6	CASABLANCA	05358522	05358522	public	\N	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit accusantium soluta quidem qui non. Odit, assumenda voluptatem debitis doloribus delectus nesciunt omnis quia unde fugit eius recusandae nisi repudiandae dolorem.	t	2014-03-14 03:55:32.553753	2014-03-14 03:55:32.553753
3	Ecole Mohamadia des Ingenieurs	EMI	admission@emi.ma	$2a$10$waCCay.5GwskwEL8E/h4g.cPeIdCp831hSdAmfGtp26OjJFYh3hFe	Avenue Al Abtal Agdal	RABAT	02365485	02659845	public	\N	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, laborum, ipsam, nulla, obcaecati quibusdam deserunt mollitia beatae maxime molestias amet porro dolor accusamus quo alias quas placeat repudiandae ab nobis?	t	2014-03-14 03:56:35.315661	2014-03-14 03:56:35.315661
\.


--
-- TOC entry 1989 (class 0 OID 0)
-- Dependencies: 172
-- Name: colleges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('colleges_id_seq', 3, true);


--
-- TOC entry 1975 (class 0 OID 24578)
-- Dependencies: 178
-- Data for Name: majors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY majors (id, major_name, college_id, created_at, updated_at) FROM stdin;
1	Computer Science	1	2014-03-15 00:23:06.200308	2014-03-15 00:23:06.200308
2	Engineering	1	2014-03-15 00:25:41.033255	2014-03-15 00:26:21.90856
3	Business and Administration	1	2014-03-15 00:32:21.294187	2014-03-15 00:32:35.952549
\.


--
-- TOC entry 1990 (class 0 OID 0)
-- Dependencies: 177
-- Name: majors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('majors_id_seq', 3, true);


--
-- TOC entry 1973 (class 0 OID 16424)
-- Dependencies: 176
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20140313170915
20140311001339
20140312011815
20140312015707
20140312021019
20140312022043
20140312163945
20140314213431
\.


--
-- TOC entry 1972 (class 0 OID 16415)
-- Dependencies: 175
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY students (id, "CIN", first_name, last_name, email, password_digest, phone, address, city, gender, birthday, "marital status", nationality, created_at, updated_at) FROM stdin;
1	\N	\N	\N	saadna@gmail.com	$2a$10$CxJgTkRSL4Ow21F5toqd2e5VLaQMi0n.o8g96fpq6YIXGIMH.qfIS	\N	\N	\N	\N	\N	\N	\N	2014-03-14 03:50:30.97855	2014-03-14 03:50:30.97855
\.


--
-- TOC entry 1991 (class 0 OID 0)
-- Dependencies: 174
-- Name: students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('students_id_seq', 1, true);


--
-- TOC entry 1852 (class 2606 OID 16401)
-- Name: administrators_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY administrators
    ADD CONSTRAINT administrators_pkey PRIMARY KEY (id);


--
-- TOC entry 1854 (class 2606 OID 16412)
-- Name: colleges_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY colleges
    ADD CONSTRAINT colleges_pkey PRIMARY KEY (id);


--
-- TOC entry 1859 (class 2606 OID 24583)
-- Name: majors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY majors
    ADD CONSTRAINT majors_pkey PRIMARY KEY (id);


--
-- TOC entry 1856 (class 2606 OID 16423)
-- Name: students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);


--
-- TOC entry 1857 (class 1259 OID 16427)
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- TOC entry 1982 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-03-15 03:11:59

--
-- PostgreSQL database dump complete
--

